import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('details', { path: '/details/:lat/:lng/:time' });
  this.route('map', { path: '/map/:lat/:lng/:time/:address'});
  this.route('history', { resetNamespace: false }, () => {
    this.route('historyView', { path: 'history/:id'});
  });
});

export default Router;
