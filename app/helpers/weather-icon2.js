import Ember from 'ember';

export function weatherIcon2(weather) {
  return new Ember.String.htmlSafe('<i class="wi wi-forecast-io-' + weather + '"></i>');
}

export default Ember.Helper.helper(weatherIcon2);
