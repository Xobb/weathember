import DS from 'ember-data';

export default DS.Model.extend({
  address: DS.attr('string'),
  lat: DS.attr(),
  lng: DS.attr(),
  at: DS.attr(),
  weather: DS.attr()
});
