import Ember from 'ember';
import hash from 'weathember/utils/hash';

export default Ember.Route.extend({
  gMap: Ember.inject.service(),
  store: Ember.inject.service(),
  actions: {
    updateSearch(address) {
      var addr = address;
      this.get('gMap').geocode({address: address}).then((geocodes) => {
        if (geocodes.length === 0) {
          alert('Location '+ addr +' was not found!');
        }

        var lat = geocodes[0].geometry.location.G, lng = geocodes[0].geometry.location.K,
          address = geocodes[0].formatted_address;
        this.transitionTo('map', lat, lng, 'empty', address);
      });
    },
    onMapClick(event) {
      this.get('gMap').geocode({ lat: event.latLng.G, lng: event.latLng.K }).then((geocodes) => {
        var lat = geocodes[0].geometry.location.G, lng = geocodes[0].geometry.location.K,
          address = geocodes[0].formatted_address;
        this.transitionTo('map', lat, lng, 'empty', address);
      });
    }
  },

  _createSearchId(obj){
    return hash(obj.lat + obj.lng);
  }

});
