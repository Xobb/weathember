import Ember from 'ember';

export default Ember.Route.extend({
  store: Ember.inject.service(),
  model(){
    return Ember.RSVP.hash({
      entries: this.get('store').findAll('search-entry')
    });
  },
  renderTemplate(){
    this.render('history/list', {
      into: 'application',
      outlet: 'weather'
    });
    this.render('history/empty', {
      outlet: 'content'
    });
  },
  actions: {
    clearHistory: function(){
      console.log('Removing history');
      this.get('store').findAll('search-entry').then((entries) => {
        entries.forEach((record) => {
          record.destroyRecord();
        });
      });
      this.transitionTo('index');
    }
  }
});
