import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function(){
    this._getCurrentPosition().then((pos) => {
      this.transitionTo('map', pos.lat, pos.lng, 'empty', 'empty');
    });
  },
  _getCurrentPosition() {
    return new Ember.RSVP.Promise((resolve, reject) => {
      if (!("geolocation" in navigator)) {
        Ember.run(null, resolve, {
          lat: 51.5072,
          lng: 0.1275
        });
      }
      navigator.geolocation.getCurrentPosition((position) => {
        Ember.run(null, resolve, {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      });
    });
  },
});
