import Ember from 'ember';

export default Ember.Route.extend({
  store: Ember.inject.service(),
  forecast: Ember.inject.service(),
  model(params){
    return Ember.RSVP.hash({
      entries: this.get('store').findAll('search-entry'),
      entry: this.get('store').find('search-entry', params.id)
    }).then((hash) => {
      console.debug(hash);
      return this.get('forecast')
        .getWeather({ lat: hash.entry.get('lat'), lng: hash.entry.get('lng'), time: hash.entry.get('at')})
        .then((weather) => {
          return this.get('forecast').convertWeatherToDetails(weather)
            .then((details) => {
              hash.entry.details = details;
              return hash;
            });
        })
    });
  },
  renderTemplate(){
    this.render('history/list', {
      into: 'application',
      outlet: 'weather'
    });
    this.render('history/view', {
      outlet: 'content'
    });
  },
  actions: {
    clearHistory: function(){
      console.log('Removing history');
      this.get('store').findAll('search-entry').then((entries) => {
        entries.forEach((record) => {
          record.destroyRecord();
        });
      });
      this.transitionTo('index');
    }
  }
});
