import Ember from 'ember';
import ApplicationRoute from './application';
import StringHash from 'weathember/utils/hash';

export default ApplicationRoute.extend({
  forecast: Ember.inject.service(),
  store: Ember.inject.service(),
  model(params){
    var model = {
      lat: params.lat,
      lng: params.lng,
      weather: this.get('forecast').getWeather({lat: params.lat, lng: params.lng}) // Resolve weather in a promise
    };
    if (params.address === 'empty') { // Resolve address if not provided
      model.address = this.get('gMap').geocode({lat: params.lat, lng: params.lng})
        .then((geocodes) => {
          return geocodes[0].formatted_address;
        });
    } else {
      model.address = params.address;
    }
    return Ember.RSVP.hash(model).then((hash) => {
      hash.zoom = 7; // Add default zoom level
      hash.markers = Ember.A([
        {
          id: 1,
          lat: hash.lat,
          lng: hash.lng,
          infoWindow: {
            content: hash.address
          }
        }
      ]);
      hash.at = moment().unix();
      hash.id = StringHash(""+hash.lat+hash.lng); // this is a helper function
      console.debug(hash);
      this.get('store').findRecord('search-entry', hash.id).then((record) => {
        record.set('at', hash.at);
        record.save();
      }, () => {
        this.get('store').createRecord('search-entry', hash).save();
      });
      return hash;
    });
  },

  renderTemplate() {
    this.render('weather', {
      into: 'application',
      outlet: 'weather'
    });
    this.render('map', {
      outlet: 'content'
    });
  },
  actions: {
    showDetails: function(time) {
      console.log('Transition!');
      this.transitionTo('details', this.get('model.lat'), this.get('model.lng'), time, this.get('model.address'));
    }
  }
});
