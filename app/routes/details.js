import Ember from 'ember';
import MapRoute from './map';

export default MapRoute.extend({
  model(params){
    return Ember.RSVP.hash({
      lat: params.lat,
      lng: params.lng,
      date: moment.unix(params.time),
      weather: this.get('forecast').getWeather({lat: params.lat, lng: params.lng}),
      address: this.get('gMap').geocode({lat: params.lat, lng: params.lng}).then((geocodes) => {
        return geocodes[0].formatted_address;
      }),
      details: this.get('forecast').getWeather(params).then((weather) => {
        return this.get('forecast').convertWeatherToDetails(weather);
      })
    });
  },
  renderTemplate() {
    this.render('weather', {
      into: 'application',
      outlet: 'weather'
    });
    this.render('details', {
      outlet: 'content'
    });
  }
});
