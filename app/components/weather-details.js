import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    showDetails: function(time){
      console.log('Call component action');
      this.sendAction("action", time);
    }
  }
});
