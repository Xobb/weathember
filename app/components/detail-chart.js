import Ember from 'ember';

export default Ember.Component.extend({
  chartOptions: {
    showArea: true,
    lineSmooth: true,
    axisX: {
      showGrid: false
    }
  }
});
