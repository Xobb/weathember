import Ember from 'ember';

export default Ember.Component.extend({
  day: Ember.computed('weather.time', function(){
    return moment.unix(this.get('weather.time')).format("ddd");
  }),
  date: Ember.computed('weather.time', function(){
    return moment.unix(this.get('weather.time')).format("MMM Do")
  })
});
