import Ember from 'ember';
import getJSON from 'weathember/utils/get-json';
import queryStringify from 'weathember/utils/query-stringify';

export default Ember.Service.extend({
  getWeather: function(obj){
    var url = "/weather.php?" + queryStringify(obj);
    return getJSON(url);
  },
  convertWeatherToDetails: function(weather){
    return new Ember.RSVP.Promise((resolve, reject) => {
      var data = {
        labels: []
      }, temperature = [], cloudCover = [], windSpeed = [];

      for (var i = 0; i < weather.data.length; i++) {
        data.labels.push(moment.unix(weather.data[i].time).format('hh a'));
        temperature.push(weather.data[i].temperature);
        cloudCover.push(weather.data[i].cloudCover * 100);
        windSpeed.push(weather.data[i].windSpeed);
      }
      data.series = [
        temperature,
        cloudCover,
        windSpeed
      ];
      Ember.run(null, resolve, data);
    });

  }

});
