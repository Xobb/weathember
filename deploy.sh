#!/usr/bin/env bash
set +x
ember build --environment production
scp -r dist/* citylance.eu:src/weathember/public_html
scp -r backend/* citylance.eu:src/weathember/public_html
