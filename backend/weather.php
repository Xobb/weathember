<?php
/**
 * Created by PhpStorm.
 * User: pavlo.chubatyy
 * Date: 10/22/15
 * Time: 15:12
 */

require __DIR__.'/vendor/autoload.php';

use MyWeather\ForecastClient;

$apiKey = '5b0f2246f49a6c728501a82be913ebbf';

function arr_get($arr, $key, $default = null) {
  if (!array_key_exists($key, $arr)) {
    return $default;
  }
  return $arr[$key];
}

function err($code, $message) {
  http_response_code($code);
  echo $message;
}

function send_response($response)
{
  header('Content-type: application/json');
  echo $response;
}

function is_dev()
{
  return array_key_exists('DEV', $_SERVER) and $_SERVER['DEV'] != 0;
}


if (is_dev() and file_exists('sample.json')) {
  $response = file_get_contents('sample.json');
  send_response($response);
  return;
}

$lat = arr_get($_GET, 'lat');
$lng = arr_get($_GET, 'lng');
$time = arr_get($_GET, 'time', null);

if (!$lat || !$lng) {
  err(400, 'Bad request');
  return;
}


$lang = 'en';
$units = 'auto';

$historical = arr_get($_GET, 't');

$client = new ForecastClient($apiKey);

/** @var \MyWeather\Forecast $forecast */
$forecast = $client->get($lat, $lng, $time);

if (!is_null($time))
  $response = $forecast->hourly();
else
  $response = $forecast->daily();

if (is_dev()) {
  $fh = fopen('sample.json', 'w');
  fwrite($fh, json_encode($response));
  fclose($fh);
}


send_response(json_encode($response));
